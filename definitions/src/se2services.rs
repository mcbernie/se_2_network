#[derive(Clone, PartialEq, ::prost::Message)]
pub struct LoginRequest {
    #[prost(string, tag="1")]
    pub uuid: std::string::String,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct LoginResponse {
    #[prost(enumeration="login_response::LoginResponseType", tag="1")]
    pub r#type: i32,
    #[prost(string, tag="2")]
    pub token: std::string::String,
    #[prost(string, tag="3")]
    pub key: std::string::String,
}
pub mod login_response {
    #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
    #[repr(i32)]
    pub enum LoginResponseType {
        Invalid = 0,
        Success = 1,
        New = 2,
    }
}
/// Ping / Pong - required to keep token alive 
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct PingRequest {
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct PingResponse {
    #[prost(enumeration="ping_response::PingResponseType", tag="1")]
    pub update: i32,
}
pub mod ping_response {
    #[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
    #[repr(i32)]
    pub enum PingResponseType {
        Nothing = 0,
        Gotupdate = 1,
    }
}
/// ask for a configuration / it could also happen that the server pushes the config to client
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ConfigRequest {
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ConfigResponse {
    #[prost(string, tag="2")]
    pub name: std::string::String,
    #[prost(string, tag="3")]
    pub anschrift: std::string::String,
    #[prost(int32, tag="4")]
    pub statistic_pause: i32,
    #[prost(int32, tag="5")]
    pub slideshow_pause: i32,
    #[prost(int32, tag="6")]
    pub show_clock: i32,
    #[prost(string, tag="7")]
    pub scrolltext: std::string::String,
    #[prost(string, tag="8")]
    pub raw_styles: std::string::String,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Media {
    #[prost(string, tag="1")]
    pub name: std::string::String,
    #[prost(string, tag="2")]
    pub uri: std::string::String,
    #[prost(int32, tag="3")]
    pub position: i32,
    #[prost(enumeration="MediaType", tag="4")]
    pub r#type: i32,
    #[prost(int32, tag="5")]
    pub duration: i32,
    #[prost(int64, tag="6")]
    pub updated_on: i64,
}
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ListResponse {
    #[prost(message, repeated, tag="1")]
    pub items: ::std::vec::Vec<Media>,
}
/// Ask for a media List / get new Medias 
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct ListRequest {
}
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash, PartialOrd, Ord, ::prost::Enumeration)]
#[repr(i32)]
pub enum MediaType {
    Image = 0,
    Video = 1,
}
pub mod client {
    use ::tower_grpc::codegen::client::*;
    use super::{LoginRequest, LoginResponse, PingRequest, PingResponse, ConfigRequest, ConfigResponse, ListRequest, ListResponse};

    #[derive(Debug, Clone)]
    pub struct Se2Services<T> {
        inner: grpc::Grpc<T>,
    }

    impl<T> Se2Services<T> {
        pub fn new(inner: T) -> Self {
            let inner = grpc::Grpc::new(inner);
            Self { inner }
        }

        /// Poll whether this client is ready to send another request.
        pub fn poll_ready<R>(&mut self) -> futures::Poll<(), grpc::Status>
        where T: grpc::GrpcService<R>,
        {
            self.inner.poll_ready()
        }

        /// Get a `Future` of when this client is ready to send another request.
        pub fn ready<R>(self) -> impl futures::Future<Item = Self, Error = grpc::Status>
        where T: grpc::GrpcService<R>,
        {
            futures::Future::map(self.inner.ready(), |inner| Self { inner })
        }

        pub fn login<R>(&mut self, request: grpc::Request<LoginRequest>) -> grpc::unary::ResponseFuture<LoginResponse, T::Future, T::ResponseBody>
        where T: grpc::GrpcService<R>,
              grpc::unary::Once<LoginRequest>: grpc::Encodable<R>,
        {
            let path = http::PathAndQuery::from_static("/se2services.Se2Services/Login");
            self.inner.unary(request, path)
        }

        pub fn ping<R>(&mut self, request: grpc::Request<PingRequest>) -> grpc::unary::ResponseFuture<PingResponse, T::Future, T::ResponseBody>
        where T: grpc::GrpcService<R>,
              grpc::unary::Once<PingRequest>: grpc::Encodable<R>,
        {
            let path = http::PathAndQuery::from_static("/se2services.Se2Services/Ping");
            self.inner.unary(request, path)
        }

        pub fn config<R>(&mut self, request: grpc::Request<ConfigRequest>) -> grpc::unary::ResponseFuture<ConfigResponse, T::Future, T::ResponseBody>
        where T: grpc::GrpcService<R>,
              grpc::unary::Once<ConfigRequest>: grpc::Encodable<R>,
        {
            let path = http::PathAndQuery::from_static("/se2services.Se2Services/Config");
            self.inner.unary(request, path)
        }

        pub fn list<R>(&mut self, request: grpc::Request<ListRequest>) -> grpc::unary::ResponseFuture<ListResponse, T::Future, T::ResponseBody>
        where T: grpc::GrpcService<R>,
              grpc::unary::Once<ListRequest>: grpc::Encodable<R>,
        {
            let path = http::PathAndQuery::from_static("/se2services.Se2Services/List");
            self.inner.unary(request, path)
        }
    }
}

pub mod server {
    use ::tower_grpc::codegen::server::*;
    use super::{LoginRequest, LoginResponse, PingRequest, PingResponse, ConfigRequest, ConfigResponse, ListRequest, ListResponse};

    // Redefine the try_ready macro so that it doesn't need to be explicitly
    // imported by the user of this generated code.
    macro_rules! try_ready {
        ($e:expr) => (match $e {
            Ok(futures::Async::Ready(t)) => t,
            Ok(futures::Async::NotReady) => return Ok(futures::Async::NotReady),
            Err(e) => return Err(From::from(e)),
        })
    }

    pub trait Se2Services: Clone {
        type LoginFuture: futures::Future<Item = grpc::Response<LoginResponse>, Error = grpc::Status>;
        type PingFuture: futures::Future<Item = grpc::Response<PingResponse>, Error = grpc::Status>;
        type ConfigFuture: futures::Future<Item = grpc::Response<ConfigResponse>, Error = grpc::Status>;
        type ListFuture: futures::Future<Item = grpc::Response<ListResponse>, Error = grpc::Status>;

        fn login(&mut self, request: grpc::Request<LoginRequest>) -> Self::LoginFuture;

        fn ping(&mut self, request: grpc::Request<PingRequest>) -> Self::PingFuture;

        fn config(&mut self, request: grpc::Request<ConfigRequest>) -> Self::ConfigFuture;

        /// Server stream all medias after requested from client
        fn list(&mut self, request: grpc::Request<ListRequest>) -> Self::ListFuture;
    }

    #[derive(Debug, Clone)]
    pub struct Se2ServicesServer<T> {
        se2_services: T,
    }

    impl<T> Se2ServicesServer<T>
    where T: Se2Services,
    {
        pub fn new(se2_services: T) -> Self {
            Self { se2_services }
        }
    }

    impl<T> tower::Service<http::Request<grpc::BoxBody>> for Se2ServicesServer<T>
    where T: Se2Services,
    {
        type Response = http::Response<se2_services::ResponseBody<T>>;
        type Error = grpc::Never;
        type Future = se2_services::ResponseFuture<T>;

        fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
            Ok(().into())
        }

        fn call(&mut self, request: http::Request<grpc::BoxBody>) -> Self::Future {
            use self::se2_services::Kind::*;

            match request.uri().path() {
                "/se2services.Se2Services/Login" => {
                    let service = se2_services::methods::Login(self.se2_services.clone());
                    let response = grpc::unary(service, request);
                    se2_services::ResponseFuture { kind: Login(response) }
                }
                "/se2services.Se2Services/Ping" => {
                    let service = se2_services::methods::Ping(self.se2_services.clone());
                    let response = grpc::unary(service, request);
                    se2_services::ResponseFuture { kind: Ping(response) }
                }
                "/se2services.Se2Services/Config" => {
                    let service = se2_services::methods::Config(self.se2_services.clone());
                    let response = grpc::unary(service, request);
                    se2_services::ResponseFuture { kind: Config(response) }
                }
                "/se2services.Se2Services/List" => {
                    let service = se2_services::methods::List(self.se2_services.clone());
                    let response = grpc::unary(service, request);
                    se2_services::ResponseFuture { kind: List(response) }
                }
                _ => {
                    se2_services::ResponseFuture { kind: __Generated__Unimplemented(grpc::unimplemented(format!("unknown service: {:?}", request.uri().path()))) }
                }
            }
        }
    }

    impl<T> tower::Service<()> for Se2ServicesServer<T>
    where T: Se2Services,
    {
        type Response = Self;
        type Error = grpc::Never;
        type Future = futures::FutureResult<Self::Response, Self::Error>;

        fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
            Ok(futures::Async::Ready(()))
        }

        fn call(&mut self, _target: ()) -> Self::Future {
            futures::ok(self.clone())
        }
    }

    impl<T> tower::Service<http::Request<tower_hyper::Body>> for Se2ServicesServer<T>
    where T: Se2Services,
    {
        type Response = <Self as tower::Service<http::Request<grpc::BoxBody>>>::Response;
        type Error = <Self as tower::Service<http::Request<grpc::BoxBody>>>::Error;
        type Future = <Self as tower::Service<http::Request<grpc::BoxBody>>>::Future;

        fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
            tower::Service::<http::Request<grpc::BoxBody>>::poll_ready(self)
        }

        fn call(&mut self, request: http::Request<tower_hyper::Body>) -> Self::Future {
            let request = request.map(|b| grpc::BoxBody::map_from(b));
            tower::Service::<http::Request<grpc::BoxBody>>::call(self, request)
        }
    }

    pub mod se2_services {
        use ::tower_grpc::codegen::server::*;
        use super::Se2Services;
        use super::super::{LoginRequest, PingRequest, ConfigRequest, ListRequest};

        pub struct ResponseFuture<T>
        where T: Se2Services,
        {
            pub(super) kind: Kind<
                // Login
                grpc::unary::ResponseFuture<methods::Login<T>, grpc::BoxBody, LoginRequest>,
                // Ping
                grpc::unary::ResponseFuture<methods::Ping<T>, grpc::BoxBody, PingRequest>,
                // Config
                grpc::unary::ResponseFuture<methods::Config<T>, grpc::BoxBody, ConfigRequest>,
                // List
                grpc::unary::ResponseFuture<methods::List<T>, grpc::BoxBody, ListRequest>,
                // A generated catch-all for unimplemented service calls
                grpc::unimplemented::ResponseFuture,
            >,
        }

        impl<T> futures::Future for ResponseFuture<T>
        where T: Se2Services,
        {
            type Item = http::Response<ResponseBody<T>>;
            type Error = grpc::Never;

            fn poll(&mut self) -> futures::Poll<Self::Item, Self::Error> {
                use self::Kind::*;

                match self.kind {
                    Login(ref mut fut) => {
                        let response = try_ready!(fut.poll());
                        let response = response.map(|body| {
                            ResponseBody { kind: Login(body) }
                        });
                        Ok(response.into())
                    }
                    Ping(ref mut fut) => {
                        let response = try_ready!(fut.poll());
                        let response = response.map(|body| {
                            ResponseBody { kind: Ping(body) }
                        });
                        Ok(response.into())
                    }
                    Config(ref mut fut) => {
                        let response = try_ready!(fut.poll());
                        let response = response.map(|body| {
                            ResponseBody { kind: Config(body) }
                        });
                        Ok(response.into())
                    }
                    List(ref mut fut) => {
                        let response = try_ready!(fut.poll());
                        let response = response.map(|body| {
                            ResponseBody { kind: List(body) }
                        });
                        Ok(response.into())
                    }
                    __Generated__Unimplemented(ref mut fut) => {
                        let response = try_ready!(fut.poll());
                        let response = response.map(|body| {
                            ResponseBody { kind: __Generated__Unimplemented(body) }
                        });
                        Ok(response.into())
                    }
                }
            }
        }

        pub struct ResponseBody<T>
        where T: Se2Services,
        {
            pub(super) kind: Kind<
                // Login
                grpc::Encode<grpc::unary::Once<<methods::Login<T> as grpc::UnaryService<LoginRequest>>::Response>>,
                // Ping
                grpc::Encode<grpc::unary::Once<<methods::Ping<T> as grpc::UnaryService<PingRequest>>::Response>>,
                // Config
                grpc::Encode<grpc::unary::Once<<methods::Config<T> as grpc::UnaryService<ConfigRequest>>::Response>>,
                // List
                grpc::Encode<grpc::unary::Once<<methods::List<T> as grpc::UnaryService<ListRequest>>::Response>>,
                // A generated catch-all for unimplemented service calls
                (),
            >,
        }

        impl<T> tower::HttpBody for ResponseBody<T>
        where T: Se2Services,
        {
            type Data = <grpc::BoxBody as grpc::Body>::Data;
            type Error = grpc::Status;

            fn is_end_stream(&self) -> bool {
                use self::Kind::*;

                match self.kind {
                    Login(ref v) => v.is_end_stream(),
                    Ping(ref v) => v.is_end_stream(),
                    Config(ref v) => v.is_end_stream(),
                    List(ref v) => v.is_end_stream(),
                    __Generated__Unimplemented(_) => true,
                }
            }

            fn poll_data(&mut self) -> futures::Poll<Option<Self::Data>, Self::Error> {
                use self::Kind::*;

                match self.kind {
                    Login(ref mut v) => v.poll_data(),
                    Ping(ref mut v) => v.poll_data(),
                    Config(ref mut v) => v.poll_data(),
                    List(ref mut v) => v.poll_data(),
                    __Generated__Unimplemented(_) => Ok(None.into()),
                }
            }

            fn poll_trailers(&mut self) -> futures::Poll<Option<http::HeaderMap>, Self::Error> {
                use self::Kind::*;

                match self.kind {
                    Login(ref mut v) => v.poll_trailers(),
                    Ping(ref mut v) => v.poll_trailers(),
                    Config(ref mut v) => v.poll_trailers(),
                    List(ref mut v) => v.poll_trailers(),
                    __Generated__Unimplemented(_) => Ok(None.into()),
                }
            }
        }

        #[allow(non_camel_case_types)]
        #[derive(Debug, Clone)]
        pub(super) enum Kind<Login, Ping, Config, List, __Generated__Unimplemented> {
            Login(Login),
            Ping(Ping),
            Config(Config),
            List(List),
            __Generated__Unimplemented(__Generated__Unimplemented),
        }

        pub mod methods {
            use ::tower_grpc::codegen::server::*;
            use super::super::{Se2Services, LoginRequest, LoginResponse, PingRequest, PingResponse, ConfigRequest, ConfigResponse, ListRequest, ListResponse};

            pub struct Login<T>(pub T);

            impl<T> tower::Service<grpc::Request<LoginRequest>> for Login<T>
            where T: Se2Services,
            {
                type Response = grpc::Response<LoginResponse>;
                type Error = grpc::Status;
                type Future = T::LoginFuture;

                fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
                    Ok(futures::Async::Ready(()))
                }

                fn call(&mut self, request: grpc::Request<LoginRequest>) -> Self::Future {
                    self.0.login(request)
                }
            }

            pub struct Ping<T>(pub T);

            impl<T> tower::Service<grpc::Request<PingRequest>> for Ping<T>
            where T: Se2Services,
            {
                type Response = grpc::Response<PingResponse>;
                type Error = grpc::Status;
                type Future = T::PingFuture;

                fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
                    Ok(futures::Async::Ready(()))
                }

                fn call(&mut self, request: grpc::Request<PingRequest>) -> Self::Future {
                    self.0.ping(request)
                }
            }

            pub struct Config<T>(pub T);

            impl<T> tower::Service<grpc::Request<ConfigRequest>> for Config<T>
            where T: Se2Services,
            {
                type Response = grpc::Response<ConfigResponse>;
                type Error = grpc::Status;
                type Future = T::ConfigFuture;

                fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
                    Ok(futures::Async::Ready(()))
                }

                fn call(&mut self, request: grpc::Request<ConfigRequest>) -> Self::Future {
                    self.0.config(request)
                }
            }

            pub struct List<T>(pub T);

            impl<T> tower::Service<grpc::Request<ListRequest>> for List<T>
            where T: Se2Services,
            {
                type Response = grpc::Response<ListResponse>;
                type Error = grpc::Status;
                type Future = T::ListFuture;

                fn poll_ready(&mut self) -> futures::Poll<(), Self::Error> {
                    Ok(futures::Async::Ready(()))
                }

                fn call(&mut self, request: grpc::Request<ListRequest>) -> Self::Future {
                    self.0.list(request)
                }
            }
        }
    }
}
