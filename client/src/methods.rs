
use futures::{Future, future};

use tower_grpc::{Request};

use crate::client::{Client, ClientMethods, ClientType};
use crate::status::{SystemState, ConnectingState, OnlineState};
use crate::se2services;
use crate::errors::RPCError;

impl ClientMethods for Client {
    // creat a thread with tokio runtime
    fn do_login(&mut self) {
        info!("call do_login");
        let config = self.get_config();
        let my_uuid = config.uuid.to_string();
        
        use se2services::LoginRequest;
        let mut login_request = LoginRequest::default();
        login_request.uuid = my_uuid.clone();
        self.connect(|mut client: ClientType| {
            client.login(Request::new(login_request))
            .and_then(|response| {
                debug!("got response on do_login: {:?}", response);
                use se2services::login_response::LoginResponseType;
                let inbound = response.into_inner();

                match LoginResponseType::from_i32(inbound.r#type).unwrap()  {
                    LoginResponseType::Success => {
                        future::ok(SystemState::Connecting(ConnectingState::LoggedIn(inbound.token)))
                    },
                    LoginResponseType::Invalid => {
                        future::ok(SystemState::Connecting(ConnectingState::LoginError("not set message".to_string())))
                    },
                    LoginResponseType::New => {
                        future::ok(SystemState::Connecting(ConnectingState::GotRegistrationKey(inbound.key)))
                    }
                }
                
            }).or_else(|e| {
                error!("got error on do_login: {:?}", e);
                future::err(
                    (
                        SystemState::Connecting(ConnectingState::LoginError(format!("{:?}",e))),
                        RPCError::RPCError(e)
                    )
                )
            })
        });
        

    }

    fn call_get_config(&mut self) {
        use se2services::ConfigRequest;      
        match self.request_with_auth(ConfigRequest::default()) {
            Some(request) => {

                self.connect(|mut client: ClientType| {
                    client.config(request)
                    .and_then(move |response| {
                        //let mut s = status.write().unwrap();
                        //s.state = SystemState::Connecting(ConnectingState::LoggedIn(inbound.token));
                        info!("got response on config: {:?}", response);
                        future::ok(SystemState::Online(OnlineState::GotConfiguration(response.into_inner().into())))
                    }).or_else(move |e| {
                        error!("got error on call_get_config: {:?}", e);
                        /*let mut s = status_on_error.write().unwrap();
                        s.state = SystemState::LoginInvalid(format!("{:?}",e));*/
                        future::err((
                            SystemState::Online(OnlineState::ErrorOnGotConfiguration),
                            RPCError::RPCError(e)
                            ))
                    })
                });

            },
            None => {
                error!("Not logged in !, cant make request");
            }
        }
        
    }

    fn call_get_list(&mut self) {
        use se2services::ListRequest;      
        use crate::list::{List, Media};
        use crate::fetch::fetch;
        use futures::prelude::*;
        use futures::stream;
        use tower_grpc::{Status, Code};
        match self.request_with_auth(ListRequest::default()) {
            Some(request) => {

                let download_status = self.get_status();

                self.connect(move |mut client: ClientType| {
                    client.list(request)
                    .and_then(move |response| {

                        let list:List = response.into_inner().into();
                        let items:Vec<Media> = list.clone().list;
                        let stream = stream::iter_ok(items);
                    
                        let new_system_state = SystemState::Online(OnlineState::GotList(list.clone()));
                        
                        stream.for_each(move |item| {
                            download_status.write().unwrap().state = SystemState::Online(OnlineState::DownloadingListItem(item.clone()));
                            fetch(&item).map_err(|_e| {
                                Status::new(Code::Unknown, "was geht ab")
                            })
                        }).and_then(|_r| {
                            future::ok(new_system_state)
                        }) 

                    }).map_err(move |e| {
                        error!("got error on call_get_list: {:?}", e);
                        /*let mut s = status_on_error.write().unwrap();
                        s.state = SystemState::LoginInvalid(format!("{:?}",e));*/
                        (
                            SystemState::Online(OnlineState::ErrorOnGotList),
                            RPCError::RPCError(e)
                        )
                    })
                });

            },
            None => {
                error!("Not logged in !, cant make request");
            }
        }
        
    }


}