use std::fmt;

pub use crate::errors::RPCError;

use crate::Configuration;
use crate::{List, Media};

#[derive(Clone, PartialEq, Debug)]
pub enum OnlineState {
    WentOnline,

    // Idle, do nothing
    Idle,

    // Configuration stuff
    AskForConfiguration,
    WaitForConfiguration,
    GotConfiguration(Configuration),
    ErrorOnGotConfiguration,
    CheckConfiguration,
    ServerSaysThereIsANewConfiguration,

    // Ping stuff
    EnablePing,
    EnabledPing,
    PingingError,

    // Media List stuff
    AskForList,
    WaitForList,
    DownloadingListItem(Media),
    GotList(List),
    ErrorOnGotList,
    ServerSaysThereIsAList,
}


#[derive(Clone, PartialEq, Debug)]
pub enum ConnectingState {
    CallDoLogin,
    DoLogin,
    LoggedIn(String),
    LoginError(String),
    GotRegistrationKey(String),
    CheckForRegistrationFinished(String),
}

#[derive(Clone, PartialEq, Debug)]
pub enum SystemState {
    Start,
    Restart,
    Connecting(ConnectingState),
    Online(OnlineState),
    ConnectionError,
}

impl fmt::Display for SystemState {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let w = match self {
            SystemState::Start => "Starting".to_string(),
            SystemState::Restart => "Restarting".to_string(),
            _ => format!("{:?}", self)
        };

        write!(f, "{}", w)
    }
}

impl Default for SystemState {
    fn default() -> Self { SystemState::Start }
}

use std::sync::Arc;

#[derive(Clone, Default)]
pub struct Status {
    pub token: Option<String>,
    pub key: Option<String>,
    pub state: SystemState,
    pub retries: usize,
    pub last_error: Option<Arc<RPCError>>,
    pub has_error: bool,
    pub connected: bool,
    pub authenticated: bool,
}

impl Status {

}


impl fmt::Display for Status {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        
        let printable_token = match &self.token {
            Some(s) => s,
            None => "NO TOKEN",
        };

        write!(f, "token:{} state:{}", printable_token, self.state)
    }
}


