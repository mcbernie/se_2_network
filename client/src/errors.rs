#![allow(dead_code)]


use tower_hyper::{client};

use std::io::Error;
use failure::Fail;
use std::boxed::Box;



pub type BoxError = Box<dyn ::std::error::Error + Send + Sync + 'static>;
pub type ConnectionError = client::ConnectError<Error>;
pub type BufError = tower_buffer::error::ServiceError;

pub type GrpcError = tower_grpc::Status;

#[derive(Debug, Fail)]
pub enum RPCError {
    #[fail(display = "Error in RPC: {}", _0)]
    RPCError(tower_grpc::Status),
    #[fail(display = "Error in RPC Buffer: {}", _0)]
    BufError(BufError),
    #[fail(display = "Error in RPC Connect: {}", _0)]
    ConnectError(ConnectionError),
    #[fail(display = "")]
    BoxError(Box<dyn ::std::error::Error + Send + Sync + 'static>),
}

impl From<ConnectionError> for RPCError {
    fn from(err: client::ConnectError<Error>) -> Self {
        RPCError::ConnectError(err)
    }
}

impl From<BoxError> for RPCError {
    fn from(err: BoxError) -> Self {
        RPCError::BoxError(err)
    }
}

impl From<tower_grpc::Status> for RPCError {
    fn from(err: tower_grpc::Status) -> Self {
        RPCError::RPCError(err)
    }
}
