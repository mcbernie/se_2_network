
use definitions::se2services;

#[derive(Clone, PartialEq, Debug)]
pub enum MediaType {
    Image,
    Video,
}
impl From<se2services::MediaType> for MediaType {
    fn from(d: se2services::MediaType) -> Self {
        match d {
            se2services::MediaType::Image => MediaType::Image,
            se2services::MediaType::Video => MediaType::Video,
        }
    }
}

#[derive(Clone, PartialEq, Debug)]
pub struct Media {
    pub uri: String,
    pub name: String,
    pub pos: i32,
    pub media_type: MediaType,
    pub duration: i32,
    pub updated_on: i64,
}
impl From<se2services::Media> for Media {
    fn from(d: se2services::Media) -> Self {
        Media {
            uri: d.uri,
            name: d.name,
            pos: d.position,
            media_type: se2services::MediaType::from_i32(d.r#type).unwrap().into(),
            duration: d.duration,
            updated_on: d.updated_on,
        }
    }
}

#[derive(Clone, PartialEq, Debug)]
pub struct List {
    pub list: Vec<Media>,
}

impl From<se2services::ListResponse> for List {
    fn from(d: se2services::ListResponse) -> Self {
        List {
            list: d.items.into_iter().map(|item| item.into()).collect()
        }
    }
}