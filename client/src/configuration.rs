

#[derive(Clone, PartialEq, Debug, Default)]
pub struct Configuration {
    pub statistic_pause: i32,
    pub slideshow_pause: i32,
    pub show_clock: i32,
    pub name: String,
    pub anschrift: String,
    pub scrolltext: String,
    pub raw_styles: String,
}

impl From<definitions::se2services::ConfigResponse> for Configuration {
    fn from(d: definitions::se2services::ConfigResponse) -> Self {
        Configuration {
            name: d.name,
            anschrift: d.anschrift,
            show_clock: d.show_clock,
            slideshow_pause: d.slideshow_pause,
            statistic_pause: d.statistic_pause,
            scrolltext: d.scrolltext,
            raw_styles: d.raw_styles,
        }
    }
}