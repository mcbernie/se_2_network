
#[macro_use]
extern crate log;

mod errors;
mod status;

mod client;
mod methods;

mod list;
mod configuration;

mod fetch;

use definitions::se2services;

pub type LoginState = se2services::login_response::LoginResponseType;

use client::{Client, ClientMethods};
pub use status::{SystemState, ConnectingState, OnlineState};

use std::time::Duration;

pub use configuration::Configuration;
pub use list::{List,Media,MediaType};


pub struct NetworkComm {
    client: Client,
    state: SystemState,
}

impl NetworkComm {
    pub fn new(uuid: uuid::Uuid, server_uri: &'static str, styles: themes::Themes) -> Self {
        let client = Client::new()
            //.set_uri("http://[::1]:50051")
            .set_uri(server_uri)
            .set_ping_interval(std::time::Duration::from_millis(5000))
            .set_uuid(uuid)
            .set_themeing(styles)
            .build();

        let state = SystemState::Start;

        NetworkComm {
            client,
            state
        }
    }

    pub fn get_state(&self) -> SystemState {
        self.state.clone()
    }

    pub fn update(&mut self) -> SystemState {
        
        let client = &mut self.client;

        let current_state = client.get_state().unwrap();
        let last_state = self.state.clone();

        
        
        if current_state != last_state || (current_state == SystemState::Start && last_state == SystemState::Start)
        {
            info!("called update. last systemstate: {:}, current systemstate: {:}", last_state, current_state);
            self.state = current_state.clone();

            match current_state.clone() {
                SystemState::Start => {
                    info!("start up, go to state : Connecting");
                    client.goto(SystemState::Connecting(ConnectingState::CallDoLogin))
                },
                SystemState::Restart => {
                    client.delayed_goto(SystemState::Start, Duration::from_millis(10000))
                }
                SystemState::ConnectionError => {
                    error!("Got an connection error, now make a delay and retry all! delay 10000 ms and go back to start");
                    client.goto(SystemState::Restart);
                },
                SystemState::Connecting(connecting_state) => {
                    match connecting_state {
                        ConnectingState::CallDoLogin => {
                            client.goto(SystemState::Connecting(ConnectingState::DoLogin));
                            client.do_login();
                        },
                        ConnectingState::DoLogin => {
                            // currently make a login...
                        },
                        ConnectingState::LoggedIn(token) => {
                            // logged in and got a token
                            client.get_status().write().unwrap().token = Some(token);
                            client.goto(SystemState::Online(OnlineState::WentOnline));
                        },
                        ConnectingState::GotRegistrationKey(key) => {
                            client.get_status().write().unwrap().key = Some(key.clone());
                            client.delayed_goto(SystemState::Connecting(ConnectingState::CheckForRegistrationFinished(key)), Duration::from_millis(5000));
                        },
                        ConnectingState::CheckForRegistrationFinished(_key) => {
                            client.do_login();
                        },
                        ConnectingState::LoginError(error_message) => {
                            error!("got error on Login {:}", error_message);
                            client.delayed_goto(SystemState::Connecting(ConnectingState::CallDoLogin), Duration::from_millis(5000));
                        }
                    };
                },

                SystemState::Online(state) => {
                    match state {
                        
                        OnlineState::WentOnline => client.goto(SystemState::Online(OnlineState::EnablePing)),
                        OnlineState::EnablePing => {
                            // enable Pinging...
                            client.enable_pinging();
                            client.goto(SystemState::Online(OnlineState::EnabledPing));
                        },
                        OnlineState::EnabledPing => client.goto(SystemState::Online(OnlineState::AskForConfiguration)),
                        OnlineState::PingingError => {
                            error!("PingingError");
                            client.goto(SystemState::Restart);
                        },
                        OnlineState::AskForConfiguration => {
                            client.goto(SystemState::Online(OnlineState::WaitForConfiguration));
                            client.call_get_config();
                        },
                        OnlineState::Idle => {
                            // Idle, do nothing, allowed is all..
                        },
                        OnlineState::CheckConfiguration => {
                            // check the fucking configuration
                        },
                        OnlineState::WaitForConfiguration => {
                            // wait for configuration result / or error
                        },
                        OnlineState::GotConfiguration(_configuration) => {
                            // got a configuration..
                            // do some config init and so thing
                            // then....
                            //client.goto(SystemState::Online(OnlineState::Idle));
                            // after receiving a new configuration, we will try to fetch a new media list..
                            info!("got a new configuraiton, now try to ask for medialist");
                            client.goto(SystemState::Online(OnlineState::AskForList));
                        },
                        OnlineState::ErrorOnGotConfiguration => {
                            error!("got an error on call configuration from server, jump to connectionerror to restart full communication");
                            client.goto(SystemState::Restart);
                        },
                        OnlineState::ServerSaysThereIsANewConfiguration => {
                            info!("Server says there is a new configuration!");
                            client.goto(SystemState::Online(OnlineState::AskForConfiguration))
                        },

                        OnlineState::ServerSaysThereIsAList => {
                            info!("Server says there is a new list!");
                            client.goto(SystemState::Online(OnlineState::AskForList))
                        },
                        OnlineState::AskForList => {
                            client.goto(SystemState::Online(OnlineState::WaitForList));
                            client.call_get_list();
                        },
                        OnlineState::WaitForList => {
                            //do nothing, wait for a list...
                        },
                        OnlineState::DownloadingListItem(media) => {
                            info!("currently downloading: {}", media.name);
                        }
                        OnlineState::GotList(_list) => {
                            // wooow received a new list... go to idle state...
                            client.goto(SystemState::Online(OnlineState::Idle));
                        },
                        OnlineState::ErrorOnGotList => {
                            error!("got an error on call list from server, jump to connectionerror to restart full communication");
                            client.goto(SystemState::Restart);
                        }

                    };
                }


            };

        }

        
        self.state.clone()

    }


}

