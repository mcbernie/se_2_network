
use std::mem;
use std::io::{copy, Cursor};
use futures::{Future, Stream};
use reqwest::r#async::{Client, Decoder};
use std::env;
use std::fs::{File, create_dir_all};
use crate::list::Media;


pub fn fetch<'a>(item: &'a Media) -> impl Future<Item=(), Error=()> {
    let filename = item.name.clone();
    let uri = item.uri.clone();
    
    Client::new()
        .get(&uri)
        .send()
        .and_then(|mut res| {
            info!("{}", res.status());

            let body = mem::replace(res.body_mut(), Decoder::empty());
            body.concat2()
        })
        .map_err(|err| error!("request error: {}", err))
        .map(|body| {
            let mut tmp_dir = env::temp_dir();

            let mut dest = {
                let fname = filename;
                println!("file to download: '{}'", &fname);
                tmp_dir.push("star-entertainer-file-upload");
                let r = create_dir_all(&tmp_dir); 
                if let Err(e) = r {
                    warn!("error on create directory for temp files: {:?}", e)
                }

                tmp_dir.push(&fname);
                println!("will be located under: '{:?}'", tmp_dir);
                File::create(tmp_dir).unwrap()
            };


            let mut body = Cursor::new(body);
            let _ = copy(&mut body, &mut dest)
                .map_err(|err| {
                    error!("stdout error: {}", err);
                }); 
        })
}