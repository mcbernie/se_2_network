use futures::{Future, Stream, future::{self, Either}};
use hyper::client::connect::{Destination, HttpConnector};

use tower_hyper::{client, util};
use tower_util::MakeService;

use tower_grpc::{BoxBody, Request};
use tower_buffer::Buffer;
use tower_request_modifier::RequestModifier;

use std::sync::{Arc, RwLock};

use tokio::timer::Delay;

use crate::status::{Status, SystemState, OnlineState};
use crate::errors::RPCError;
use crate::se2services;

use std::time::{Duration, Instant};

#[allow(dead_code)]
type Buf = Buffer<RequestModifier<tower_hyper::client::Connection<BoxBody>, BoxBody>, http::Request<BoxBody>>;

#[allow(dead_code)]
pub type ClientType = se2services::client::Se2Services<Buf>;

#[derive(Default)]
pub struct ClientBuilder {
    server_uri: Option<http::Uri>,
    ping_interval: Option<std::time::Duration>,
    uuid: Option<uuid::Uuid>,
    styles: Option<themes::Themes>,
}

#[derive(Clone)]
pub struct Config {
    pub server_uri: http::Uri,
    ping_interval: Option<std::time::Duration>,
    pub uuid: uuid::Uuid,
}

pub struct Client {
    config: Config,
    status: Arc<RwLock<Status>>,
    pub thread_pool: tokio::runtime::Runtime,
}


pub trait ClientMethods {
    fn do_login(&mut self);
    fn call_get_config(&mut self);
    fn call_get_list(&mut self);
}

impl Client {
    pub fn new() -> ClientBuilder {
        ClientBuilder::new()
    }

    pub fn request_with_auth<T>(&mut self, request: T) -> Option<Request<T>> {
        
        let s = self.status.read().unwrap();
        let uuid = self.config.uuid.to_string();
        
        let token = s.token.as_ref().map_or("", |n| n);

        match s.state {
            SystemState::Online(_) if s.token.is_some() => {
                let mut r = Request::new(request);                    
                let metadata = r.metadata_mut();
                metadata.insert("x-uuid", uuid.parse().unwrap());
                metadata.insert("x-secure-token", token.parse().unwrap());

                Some(r)
            }, 
            _ => {
                None
            }
        }

    }

    pub fn enable_pinging(&mut self) {
        //let status_for_take_while = status.clone();
        let status = self.get_status();
        let config = self.get_config();

        
        if config.ping_interval.is_none() {
            warn!("want to enable_pinging, but ping_interval is none!");
            return;
        }

        use tokio::timer::Interval;
        use std::time::{Instant};
        
        let do_ping = Interval::new(Instant::now(), config.clone().ping_interval.unwrap())
            .map_err(|e| panic!("timer error: {:?}", e))
            .for_each(move |_res| {
                let status_on_error = status.clone();
                let status_on_response = status.clone();
                let uuid = config.uuid.to_string();
                let s = status.read().unwrap();
                match s.state.clone() {
                    SystemState::Online(OnlineState::Idle) if s.token.is_some() => {

                        let token = s.clone().token.as_ref().map_or("", |n| n).to_string();
                        info!("ping server with token {}", token);
                        Either::A(

                            Client::create_client(config.server_uri.clone())
                            .map_err(|e| {
                                tower_grpc::Status::new(tower_grpc::Code::Unknown, format!("{:?}", e))
                            })
                            .and_then(move |mut client| {
                                use se2services::PingRequest;
                                let ping_request = PingRequest::default();
                                let mut request = Request::new(ping_request);
                                
                                let metadata = request.metadata_mut();
                                metadata.insert("x-uuid", uuid.parse().unwrap());
                                metadata.insert("x-secure-token", token.parse().unwrap());

                                client.ping(request)
                            })
                            .and_then(move |response| {
                                debug!("got valid ping response: {:?}", response);
                                use se2services::ping_response::PingResponseType;
                                let inbound = response.into_inner();

                                match PingResponseType::from_i32(inbound.update).unwrap()  {
                                    PingResponseType::Gotupdate => {
                                        info!("got ping response with update informations!");
                                        let mut s = status_on_response.write().unwrap();
                                        s.state = SystemState::Online(OnlineState::ServerSaysThereIsANewConfiguration);
                                    },
                                    _ => {}
                                };


                                Ok(())
                            }).map_err(move |e| {
                                // Hier muss später auch ein disaster verhindert werden
                                warn!("got error on ping: {:?}", e);
                                let mut s = status_on_error.write().unwrap();
                                s.last_error = Some(Arc::new(RPCError::RPCError(e)));
                                s.state = SystemState::Online(OnlineState::PingingError);
                                
                            })
                            
                        )

                    },
                    _ => {
                        info!("i think i cloud not ping, cause not idle or token is empty... retry it the next time...");
                        Either::B(
                            future::ok(())
                        )
                    }
                }
            
            });
            
        self.thread_pool.spawn(do_ping.map_err(|e| {
            panic!("interval errored; err={:?}", e);
        }));
    }

    pub fn delayed_goto(&mut self, nextstate: SystemState, duration: Duration){
        let when = Instant::now() + duration;
        let status = self.get_status();
        //let status_for_begin = self.get_status();
        

        /*{
            let mut c = status_for_begin.write().unwrap();
            c.state = SystemState::Delayed(Box::new(nextstate.clone()));
        }*/

        let d = Delay::new(when)
        .map_err(|e| panic!("delayed timer failed; err={:?}", e))
        .and_then(move |_| {
            let mut status = status.write().unwrap();
            status.state = nextstate;
            Ok(())
        });

        self.thread_pool.spawn(d);
    }

    pub fn goto(&mut self, nextstate: SystemState) {
        let status = self.get_status();
        let mut c = status.write().unwrap();
        c.has_error = false;
        c.state = nextstate;
    }

    pub fn get_state(&self) -> Option<SystemState> {
        let lock = self.status.read().unwrap();
        let status = (*lock).clone();
        Some(status.state)
    }

    pub fn get_config(&self) -> Config {
        self.config.clone()
    }

    pub fn get_status(&self) -> Arc<RwLock<Status>> {
        self.status.clone()
    }

    pub fn connect<F, R>(&mut self, f: F) 
    where 
        F: FnOnce(ClientType) -> R + Sized + Send + 'static, 
        R: futures::future::Future<Item = SystemState, Error = (SystemState, RPCError)> + Sized + Send + 'static,
    {
        let status = self.get_status();
        let status_for_error = self.get_status();
        let config = self.get_config();

        let cli = Client::create_client(config.server_uri.clone());


        let connection = cli
            .map_err(|create_client_error| {
                error!("general client connection error...: {:?}", create_client_error);
                (
                    SystemState::ConnectionError,
                    create_client_error
                )
            })
            .and_then(move |client| {
                f(client)
            }).and_then(move |state| {
                let mut s = status.write().unwrap();
                s.last_error = None;
                s.state = state;

                debug!("Hier ist ein okay... dann ist doch alles gut!");
                future::ok(())
            })
            .map_err(move |error_state| {
                warn!("got error on call function map_err on connect call: {:?}", error_state.1);
                let mut s = status_for_error.write().unwrap();
                s.last_error = Some(Arc::new(error_state.1));
                s.state = error_state.0;

            });
            
        self.thread_pool.spawn(connection);
    }

    pub fn create_client(server_uri: http::Uri) 
        -> impl Future<Item = ClientType, Error = RPCError> + Send + 'static {

        let uri = server_uri.clone();
        let dst = Destination::try_from_uri(uri.clone()).unwrap();
        let connector = util::Connector::new(HttpConnector::new(4));
        let settings = client::Builder::new().http2_only(true).clone();
        let mut make_client = client::Connect::with_builder(connector, settings);

        make_client.make_service(dst.clone())
            .map_err(|error| {
                RPCError::ConnectError(error)
            })
            .and_then(move |connected| {
                use se2services::client::Se2Services;
                let conn = tower_request_modifier::Builder::new()
                    .set_origin(uri.clone())
                    .build(connected)
                    .unwrap();
                
                let buff = Buffer::new(conn, 128);
                Se2Services::new(buff)
                    .ready()
                    .map_err(|e| {
                        RPCError::RPCError(e)
                    })
            })
        
    }


    pub fn check_config(&mut self) {
        use std::env;
        use std::fs::File;

        let mut dir = env::temp_dir();

        dir.push("config.txt");
        info!("check config need refresh");
        if dir.exists() {
            if let Ok(meta) = dir.metadata() {
                if meta.created().unwrap().elapsed().unwrap().as_secs() > 20 ||
                   meta.modified().unwrap().elapsed().unwrap().as_secs() > 20  {
                       info!("config need refresh, created / modified is older than 20 seconds");
                       self.call_get_config();
                } else {
                    info!("config does not need refresh, created / modified is newer than 20 seconds");    
                }
            }

        } else {
             info!("config file does not exists...");
            self.call_get_config();
        }
    }



}


impl ClientBuilder {
    pub fn new() -> Self {
        ClientBuilder {
            ..Default::default()
        }
    }

    #[allow(dead_code)]
    pub fn set_something(self) -> Self {
        self
    }

    pub fn set_ping_interval(mut self, duration: std::time::Duration) -> Self {
        self.ping_interval = Some(duration);
        self
    }

    pub fn set_uri(mut self, uri: &'static str) -> Self {
        self.server_uri = Some(parse_str_to_uri(uri));
        self
    }

    #[allow(dead_code)]
    pub fn set_uuid(mut self, uid: uuid::Uuid) -> Self {
        self.uuid = Some(uid);
        self
    }

    pub fn set_themeing(mut self, styles: themes::Themes) -> Self {
        self.styles = Some(styles);
        self
    }

    pub fn build(self) -> Client {
        let server_uri = self.server_uri.unwrap_or(parse_str_to_uri("https://127.0.0.1:50051"));

        let thread_pool = create_runtime();
        //create_runtime();

        let status = Status{
            ..Default::default()
        };

        let uuid = match self.uuid {
            Some(u) => u,
            None => uuid::Uuid::new_v4(),
        };

        let config = Config {
            server_uri,
            ping_interval: self.ping_interval,
            uuid,
        };

        Client {
            config,
            thread_pool,
            status: Arc::new(RwLock::new(status)),
        }
    }
}

fn create_runtime() -> tokio::runtime::Runtime {
    debug!("create tokio future runtime");
    tokio::runtime::Builder::new()
        .before_stop(|| {
            debug!("tokio::runtime - thread stopping");
        })
        .build().unwrap()
}


fn parse_str_to_uri(uri: &'static str) -> http::Uri {
    format!("{}", uri).parse().unwrap()
}


#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_client_builder() {
        let c = Client::new()
        .set_uri("http://192.168.0.1:5001")
        .build();
    
        assert_eq!(c.config.server_uri, "http://192.168.0.1:5001");

        let c = Client::new()
        .build();
    
        assert_eq!(c.config.server_uri, "https://127.0.0.1:50051");
    }

}