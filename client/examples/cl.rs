extern crate client;

#[macro_use]
extern crate log;

use dotenv::dotenv;

use std::thread::{sleep};
use std::time::Duration;

fn main() {
    // test builder
    dotenv().ok();
    let _ = ::env_logger::init();
    
    //let uuid = uuid::Uuid::new_v4();
    let uuid = uuid::Uuid::parse_str("3eafefc3-d548-4cf3-9476-012545cb96b3").unwrap();

    info!("init network client with uuid {}", uuid);

    let mut nc = client::NetworkComm::new(uuid, "http://127.0.0.1:50051");

    let run = true;
    while run {
        sleep(Duration::from_millis(2000));
        nc.update();
    }
    
}

