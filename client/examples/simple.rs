#![deny(warnings, rust_2018_idioms)]

use futures::Future;
use hyper::client::connect::{Destination, HttpConnector};
use tower_grpc::Request;
use tower_hyper::{client, util};
use tower_util::MakeService;

use definitions::se2services;

pub fn main() {
    let _ = ::env_logger::init();

    let uri: http::Uri = format!("http://[::1]:50051").parse().unwrap();

    let dst = Destination::try_from_uri(uri.clone()).unwrap();
    let connector = util::Connector::new(HttpConnector::new(4));
    let settings = client::Builder::new().http2_only(true).clone();
    let mut make_client = client::Connect::with_builder(connector, settings);

    let do_login = make_client
        .make_service(dst)
        .map_err(|e| panic!("connect error: {:?}", e))
        .and_then(move |conn| {
            use se2services::client::Se2Services;
            let conn = tower_request_modifier::Builder::new()
                .set_origin(uri)
                .build(conn)
                .unwrap();

            // Wait until the client is ready...
            Se2Services::new(conn).ready()
        })
        .and_then(|mut client| {
            use se2services::LoginRequest;
            let mut login_request = LoginRequest::default();
            login_request.uuid = "UUID SUPER MASTER".to_string();

            let mut request = Request::new(login_request);

            request
                .metadata_mut()
                .insert("x-secure-token", "abcdefghkdifsdsoijrgoijdsoijgroiejgoijferrggiojegr".parse().unwrap());

            client.login(request)
        })
        .and_then(|response| {
            println!("RESPONSE = {:?}", response);
            Ok(())
        })
        .map_err(|e| {
            println!("ERR = {:?}", e);
        });

    tokio::run(do_login);
}