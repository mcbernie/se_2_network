#![allow(non_snake_case)]

use crate::schema::{sms_halle, devices, media};


#[derive(Identifiable, Queryable)]
#[table_name = "sms_halle"]
pub struct Halle {
    pub id: i32,
    pub betreiberID: i32,
    pub anschrift: String,
    pub name: String,
    pub anschlussdatum: Option<chrono::NaiveDateTime>,
    pub vertragsnr: String,
    pub tjms_hallenID: Option<i32>,
    pub tjms_kundenID: Option<i32>,
    pub mediaKatID: Option<i32>,
    pub ort: String,
    pub plz: String,
}

#[derive(Identifiable,Queryable, Associations, Clone, Debug)]
#[table_name = "devices"]
#[belongs_to(Halle)]
pub struct Device {
    pub id: i32,
    pub uuid: Option<String>,
    pub key: Option<String>,
    pub token: Option<String>,
    pub token_valid_thru: Option<chrono::NaiveDateTime>,
    pub last_update: Option<chrono::NaiveDateTime>,
    pub last_connection: Option<chrono::NaiveDateTime>,
    pub registration_on: Option<chrono::NaiveDateTime>,
    pub halle_id: Option<i32>,
    pub state: Option<i32>,
    pub statistic_pause: Option<i32>,
    pub slideshow_pause: Option<i32>,
    pub show_clock: i32,
    pub scrolltext: Option<String>,
    pub style: Option<String>,
}

#[derive(QueryableByName, Identifiable,Queryable, Associations, Clone, Debug, Hash)]
#[table_name = "media"]
#[primary_key(MediaID)]
pub struct Media {
    pub MediaID: i32,
    pub MediaName: String,
    pub Dateiname: String,
    pub Groesse: i32,
    pub DefaultAnzeigedauerInSek: i32,
    pub Format: i32,
    pub Landscape: i32,
}
