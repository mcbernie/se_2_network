#[macro_use]
extern crate diesel;
extern crate dotenv;
extern crate chrono;
extern crate md5;

extern crate futures;

#[macro_use]
extern crate log;

extern crate r2d2;

use diesel::prelude::*;
use diesel::mysql::MysqlConnection;

use r2d2::{Pool, PooledConnection};
use diesel::r2d2::ConnectionManager;
//use diesel::Connection;

use dotenv::dotenv;
use std::env;

mod key_generator;
mod schema;
pub mod models;


pub use models::{Device, Halle, Media};
pub use r2d2::Error;


pub type DbPool = Pool<ConnectionManager<MysqlConnection>>;
pub type DbResult = Result<DatabaseResult, DatabaseErrors>;
pub type DbConn = PooledConnection<ConnectionManager<MysqlConnection>>;

pub enum UpdateCheckerResult {
    DataInDatabaseAreNewer,
    NoUpdatesAvailable,
    DataInDatabaseAreOlder
}


#[derive(PartialEq, Debug)]
pub enum MakeInitResult {
    Valid(i32, String),
    New(String),
}
impl From<MakeInitResult> for DatabaseResult {
    fn from(e: MakeInitResult) -> Self {
        DatabaseResult::MakeInitResult(e)
    }
}

#[derive(Debug)]
pub enum DatabaseErrors {
    CouldNotConnect,
    TokenIsInvalid,
    TokenIsExpired,
    LocationNotFound,
    DeviceNotFound,
    DeviceIsBlocked,
    SomethingElse(String),
    R2D2Error(r2d2::Error),
    ResultError(diesel::result::Error),
}
impl From<diesel::result::Error> for DatabaseErrors {
    fn from(e: diesel::result::Error) -> Self {
        DatabaseErrors::ResultError(e)
    }
}
impl From<r2d2::Error> for DatabaseErrors {
    fn from(er: r2d2::Error) -> Self {
        DatabaseErrors::R2D2Error(er)
    }
}

#[derive(Debug)]
pub enum DatabaseResult {
    TokenIsValid,
    MakeInitResult(MakeInitResult),
    DatabaseErrors(DatabaseErrors),
}
impl From<DatabaseErrors> for DatabaseResult {
    fn from(er: DatabaseErrors) -> Self {
        DatabaseResult::DatabaseErrors(er)
    }
}


pub struct DatabasePool {
    pub pool: DbPool,
}

fn create_pool() -> DbPool {
    info!("setup database connection Manager");
    dotenv().ok();
    let database_url = env::var("DATABASE_URL")
        .expect("DATABASE_URL must be set");
    //let config = Config::default();

    // Create a connection pool manager for a Postgres connection at the `database_url`
    let manager = ConnectionManager::<MysqlConnection>::new(database_url);

    // Create the pool with the default config and the r2d2_diesel connection manager
    Pool::new(manager).expect("Failed to create pool.")
}

impl DatabasePool {
    pub fn new() -> Self {
        let pool = create_pool();
        DatabasePool {
            pool,
        }
    }

    pub fn get(&self) -> DbPool {
        self.pool.clone()
    }


    fn call_pool<F,T>(&self, f: F) -> Result<T,DatabaseErrors>
    where F: FnOnce(&DbConn) -> Result<T,DatabaseErrors>,
    {
        match self.connection() {
            Ok(conn) => f(&conn),
            Err(e) => Err(e),
        }
    }  

    fn connection(&self) -> Result<DbConn, DatabaseErrors> {
        match self.pool.get() {
            Ok(connection) => {
                Ok(connection)
            },
            Err(e) => {
                error!("could not retrieve a connection from pool! {:}", e);
                Err(e.into())
            }
        }
    }

    pub fn check_if_token_is_valid(&self, device_id: i32, device_token: &str) -> DbResult {
        debug!("check_if_token_is_valid got called for device: {:}", device_id);
        self.call_pool(|connection| {
            use schema::devices::dsl::*;
            match devices.find(device_id).get_result::<Device>(connection) {
                Ok(device) => {
                    if let Some(d_token) = device.token {
                        if d_token == device_token {
                            if let Some(valid_thru) = device.token_valid_thru {
                                use chrono::prelude::*;
                                if valid_thru < Utc::now().naive_utc() {
                                    Err(DatabaseErrors::TokenIsExpired)
                                } else {
                                    self.refresh_valid_token(device_id)
                                }
                            } else {
                                Err(DatabaseErrors::TokenIsInvalid)
                            }
                        } else {
                            Err(DatabaseErrors::TokenIsInvalid)
                        }
                    } else {
                        Err(DatabaseErrors::TokenIsInvalid)
                    }
                },
                Err(e) => Err(e.into())
            }
        })
    }

    ///set a new valid_thru date for device token
    fn refresh_valid_token(&self, device_id: i32) -> DbResult {
        debug!("refresh_valid_token is called... for device_id: {:}", device_id);
        self.call_pool(|connection| {
            use schema::devices::dsl::*;

            if let Ok(device) = devices.find(device_id).get_result::<Device>(connection) {
                use chrono::prelude::*;
                use chrono::Duration;

                let valid_thru = (Utc::now() + Duration::minutes(5)).naive_utc();
                
                let res = diesel::update(&device)
                    .set(token_valid_thru.eq(valid_thru))
                    .execute(connection);

                if let Err(e) = res {
                    Err(e.into())
                } else {
                    Ok(DatabaseResult::TokenIsValid)
                }

            } else {
                warn!("device not found");
                Err(DatabaseErrors::DeviceNotFound)
            }

        })
    }

    /// check_token checks if the token for uuid is valid
    pub fn check_token(&self, uuid_value: String, token: String) -> DbResult {
        self.call_pool(|connection| {
            match get_location_by_uuid(&connection, uuid_value.clone()) {
                Ok((_location, device)) => {
                    //check if location is valid (means status is valid == null)
                    //then check if token is okay and the valid thru date is not overdingens
                    if let Some(d_token) = device.token {
                        if d_token == token {
                            if let Some(valid_thru) = device.token_valid_thru {
                                use chrono::prelude::*;
                                if valid_thru < Utc::now().naive_utc() {
                                    Err(DatabaseErrors::TokenIsExpired)
                                } else {
                                    self.refresh_valid_token(device.id)
                                }
                            } else {
                                warn!("token token_valid_thru in database is not set!");
                                Err(DatabaseErrors::TokenIsInvalid)
                            }
                        } else {
                            warn!("token in database are not equal!");
                            Err(DatabaseErrors::TokenIsInvalid)
                        }
                    } else {
                        warn!("token in database is empty");
                        Err(DatabaseErrors::TokenIsInvalid)
                    }
                },
                Err(e) => {
                    warn!("location for device not found! {:?}", e);
                    Err(DatabaseErrors::LocationNotFound)
                }
            }
        })
    
    }

    pub fn set_time_to_old(&self, uuid_value: String) {
        let _r = self.call_pool(|connection| {
            match get_location_by_uuid(connection,uuid_value.clone()) {
                Ok((_location, device)) => {
                    use schema::devices::dsl::*;
                    use chrono::prelude::*;
                    use chrono::Duration;

                    let valid_thru = (Utc::now() - Duration::minutes(1)).naive_utc();
                    
                    let res = diesel::update(&device).set(token_valid_thru.eq(valid_thru)).execute(connection);
                    if let Err(e) = res {
                        error!("error on update validtoken: {:?}", e)
                    } else {
                        info!("set valid thru token to old datetime (now - 1 min)");
                    }
                }, 
                Err(_) => {
                    warn!("error on set time to old time");
                }
            };

            // we do not need a result return
            Ok(())
        });


    }

    pub fn get_device_by_token(&self, token_str: &str) -> Result<Device, DatabaseErrors> {
        self.call_pool(|cn| {
            use schema::devices::dsl::*;
            devices.filter(token.eq(token_str)).first::<Device>(cn).map_err(|e| e.into())
        })
    }


    pub fn update_device(&self, device_id: i32) {
       let _r = self.call_pool(|cn| {
            use chrono::Utc;
            use schema::devices::dsl::*;
            debug!("update device: {:}", device_id);
            diesel::update(devices.find(device_id))
                .set(last_connection.eq(Utc::now().naive_utc()))
                .execute(cn)
                .expect(&format!("Unable to find device {}", device_id));
            Ok(())
        });
    }

    pub fn check_for_updates(&self, device_id: i32) -> Result<UpdateCheckerResult, DatabaseErrors> {
        self.call_pool(|cn| {
            match get_device(device_id, cn) {
                Ok(device) => {
                    match device.last_connection {
                        Some(l_connection) => {
                            match device.last_update {
                                Some(l_update) => {
                                    if l_update > l_connection {
                                        Ok(UpdateCheckerResult::DataInDatabaseAreNewer)
                                    } else {
                                        Ok(UpdateCheckerResult::DataInDatabaseAreOlder)
                                    }
                                },
                                None => Ok(UpdateCheckerResult::NoUpdatesAvailable)
                            }
                        }, 
                        None => Ok(UpdateCheckerResult::NoUpdatesAvailable)
                    }
                },
                Err(e) => Err(e.into())

            }
        })
    }

    pub fn get_device(&self, device_id: i32) -> Result<Device, DatabaseErrors> {
        self.call_pool(|cn| {
            match get_device(device_id, cn) {
                Ok(device) => Ok(device),
                Err(e) => Err(e.into()),
            }
        })
    }  

    pub fn get_location(&self, device_id: i32) -> Result<Halle, DatabaseErrors> {
        self.call_pool(|cn| {
            match get_device(device_id, cn) {
                Ok(device) => {
                    match device.halle_id {
                        Some(hid) => {
                            match get_location(hid, cn) {
                                Ok(location) => Ok(location),
                                Err(e) => Err(e.into()),
                            }
                        },
                        None => {
                            Err(DatabaseErrors::LocationNotFound)
                        }
                    }
                }, 
                Err(e) => Err(e.into())
            }
        })
    }

    pub fn make_init(&self, uuid_value: String) -> DbResult {
        self.call_pool(|connection| {

            //use schema::devices::dsl::*;

            let uuid_value_for_query = uuid_value.clone();

            match find_device_by_uuid(uuid_value_for_query, &connection) {
                Ok(r) => {
                    // if key is set, then registration is not complete
                    // else if key is not set and halle is associated, registration is complete, generate new token and return token
                    // if kes is not set, and no halle is accociated return blocked
                    // if entry not exists, new
                    match r.key {
                        Some(ref key_field) if key_field.len() == 0 => {
                            get_token_on_init(uuid_value.clone(),&connection)
                        },
                        Some(ref key_field) if key_field.len() < 10 => {
                            error!("WRONG KEY LENGTH!");
                            Err(DatabaseErrors::DeviceIsBlocked)
                        },
                        Some(key_field) => {
                            Ok(MakeInitResult::New(key_field.to_string()).into())

                        },
                        None => {
                            get_token_on_init(uuid_value.clone(),&connection)
                        }
                    }
                },
                Err(e) => {
                    error!("ERROR:{:}", e);
                    use diesel::insert_into;
                    use schema::devices::dsl::*;
                    // entry not found?, so we need to add it to the database... and a new key
                    info!("new registration progress initiatet");
                    let key_str = key_generator::generate_key();
                    let result = insert_into(devices)
                        .values((uuid.eq(uuid_value), key.eq(key_str.clone())))
                        .execute(connection);
                    
                    if let Err(e) = result {
                        error!("Error on insert device! {}", e);
                        Err(e.into())
                    } else {
                        Ok(MakeInitResult::New(key_str).into())
                    }
                    
                }
            }

        })
    }  

    ///activate_by_location activate a location with the given device with the same key 
    pub fn activate_by_location(pool: DbPool, hallen_id: i32, key: String) -> Result<(),String> {
        let connection = pool.get().unwrap();

        use schema::sms_halle::dsl::*;
        match sms_halle.find(hallen_id).first::<Halle>(&connection) {
            Ok(_halle) => {
                match find_device_by_key(key, &connection) {
                    Ok(device) => {
                        match add_halle_to_device(&device, hallen_id, &connection) {
                            Ok(_) => {
                                Ok(())
                            }, 
                            Err(_) => {
                                Err("Error on updateing key".to_string())
                            }
                        }
                    },
                    Err(_) => {
                        Err("cannot find device by given key".to_string())
                    }
                }
            },
            Err(e) => {
                Err(format!("Halle not found!: {}", e))
            }
        }
    }


    pub fn get_media(&self, hallen_nr: i32, kunden_nr: i32) -> Result<Vec<Media>, DatabaseErrors> {

        self.call_pool(|conn| {
            diesel::sql_query("select media.* from media 
                            left join mediakatassign on mediakatassign.MediaKatID = media.MediaKatID
                            left join mediakatselecthalle on mediakatselecthalle.MediaKatID = mediakatassign.MediaKatID and mediakatselecthalle.KundenNr = mediakatassign.KundenNr
                            where 
                            mediakatassign.KundenNr = ? and 
                            mediakatselecthalle.HallenNr = ? and 
                            (DefaultBenutzeDatumGrenze=0 or (DefaultBenutzeDatumGrenze = 1 and DefaultStartDatum >= Now() and DefaultEndDatum <= Now())) 
                            and Geloescht = 0
                            and Format = 2 and Landscape = 1
                            and not MediaName in ('smt_frage', 'smt_daten', 'smt_frage_dummy')")
            .bind::<diesel::sql_types::Integer, _>(kunden_nr)                            
            .bind::<diesel::sql_types::Integer, _>(hallen_nr)
            .load(conn).map_err(|e| e.into())
        })
    }

    pub fn get_media_hash(&self, hallen_nr: i32, kunden_nr: i32) -> Result<u64, DatabaseErrors> {
        match self.get_media(hallen_nr, kunden_nr) {
            Ok(medias) => {
                Ok(calculate_hash(&medias))
            },
            Err(e) => Err(e),
        }
    }

}

fn get_location_by_uuid(cn: &DbConn, uuid_value: String) -> Result<(Halle, Device),DatabaseErrors>  {
    match find_device_by_uuid(uuid_value.clone(), cn) {
        Ok(device) => {
            match device.halle_id {
                Some(halle_id) => {
                    match get_location(halle_id, cn) {
                        Ok(l) => {
                          Ok((l, device))  
                        },
                        Err(_) => {
                            Err(DatabaseErrors::LocationNotFound)
                        }
                    }
                },
                None => {
                    Err(DatabaseErrors::LocationNotFound)
                }
            }
        },
        Err(e) => {
            Err(e.into())
        }
    }
}

fn get_device(device_id: i32, cn: &DbConn) -> QueryResult<Device> {
    use schema::devices::dsl::*;
    devices.find(device_id).get_result::<Device>(cn)
}

fn get_location(hallen_id: i32, cn: &DbConn) -> QueryResult<Halle> {
    use schema::sms_halle::dsl::*;
    sms_halle.find(hallen_id).first::<Halle>(cn)
}

fn find_device_by_uuid(uuid_value: String, cn: &DbConn) -> QueryResult<Device> {
    use schema::devices::dsl::*;
    devices.filter(uuid.eq(uuid_value)).first::<Device>(cn)
}



/// generate_token generates a new token for the given uuid
fn generate_token(uuid_value: String, cn: &DbConn) -> Result<(i32, String), DatabaseErrors> {
    match find_device_by_uuid(uuid_value.clone(), cn) {
        Ok(device) => {
            match device.halle_id {
                Some(halle_id) => {
                    match get_location(halle_id, cn) {
                        Ok(_) => {
                            use schema::devices::dsl::*;
                            use chrono::prelude::*;
                            use chrono::Duration;

                            let f_token = key_generator::generate_token(uuid_value.clone());
                            let valid_thru = (Utc::now() + Duration::minutes(5)).naive_utc();

                            let ret = diesel::update(&device).set((token.eq(f_token.clone()), token_valid_thru.eq(valid_thru))).execute(cn);
                            if let Err(e) = ret {
                                Err(e.into())
                            } else {
                                 Ok((device.id, f_token))
                            }
                           
                        },
                        Err(_) => {
                            Err(DatabaseErrors::LocationNotFound)
                        }
                    }
                },
                None => {
                    Err(DatabaseErrors::LocationNotFound)
                }
            }
            
        }, 
        Err(_) => {
            Err(DatabaseErrors::DeviceNotFound)
        }
    }
    
}

/// get_token_on_init crate a new token on init for the given uuid
fn get_token_on_init(uuid_value: String, cn: &DbConn) -> DbResult {
    match generate_token(uuid_value, cn) {
        Ok((db_id, token)) => Ok(Into::into(MakeInitResult::Valid(db_id, token))),
        Err(e) => Err(e.into())
    }
}

fn find_device_by_key(key_value: String, cn: &DbConn) -> QueryResult<Device> {
    use schema::devices::dsl::*;
    devices.filter(key.eq(key_value)).first::<Device>(cn)
}

fn add_halle_to_device(device: &Device, h_id: i32, cn: &DbConn) -> QueryResult<usize> {
    use schema::devices::dsl::*;
    diesel::update(device).set((key.eq(""), halle_id.eq(h_id))).execute(cn)
}

use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};

fn calculate_hash<T: Hash>(t: &T) -> u64 {
    let mut s = DefaultHasher::new();
    t.hash(&mut s);
    s.finish()
}