#![allow(non_snake_case)]

table! {
    sms_halle (id) {
        id -> Integer,
        betreiberID -> Integer,
        anschrift -> Text,
        name -> Text,
        anschlussdatum -> Nullable<Timestamp>,
        vertragsnr -> Text,
        tjms_hallenID -> Nullable<Integer>,
        tjms_kundenID -> Nullable<Integer>,
        mediaKatID -> Nullable<Integer>,
        ort -> Text,
        plz -> Text,
    }
}

table! {
    devices (id) {
        id -> Integer,
        uuid -> Nullable<Text>,
        key -> Nullable<Text>,
        token -> Nullable<Text>,
        token_valid_thru -> Nullable<Timestamp>,
        last_update -> Nullable<Timestamp>,
        last_connection -> Nullable<Timestamp>,
        registration_on -> Nullable<Timestamp>,
        halle_id -> Nullable<Integer>,
        state -> Nullable<Integer>,
        statistic_pause -> Nullable<Integer>,
        slideshow_pause -> Nullable<Integer>,
        show_clock -> Integer,
        scrolltext -> Nullable<Text>,
        style -> Nullable<Text>,
    }
}

table! {
    media (MediaID) {
        MediaID -> Integer,
        MediaName -> Text,
        Dateiname -> Text,
        Groesse -> Integer,
        DefaultAnzeigedauerInSek -> Integer,
        Format -> Integer,
        Landscape -> Integer,
    }
}
