extern crate database;
extern crate diesel;

use self::database::*;
use self::models::*;
use self::diesel::prelude::*;

fn main() {
    use database::schema::sms_halle::dsl::*;

    let connection = establish_connection();
    
    /***
    let results = sms_halle.filter(tjms_kundenID.eq(1))
        .limit(5)
        .load::<Halle>(&connection)
        .expect("Error loading sms_halle");

    println!("Displaying {} sms_halle", results.len());
    for halle in results {
        println!("{}, {}, {}", halle.id, halle.name, halle.anschrift);     
    }
    ***/

    let my_uuid = "ABCDEFG".to_string();


    println!("Test query for init");

    let r = database::make_init(&connection, my_uuid.clone()); // so begin with make an init request
 
    let t = match r.unwrap() {
        database::MakeInitResult::Blocked => {
            println!("Blocked !");
            None
        },
        database::MakeInitResult::Valid(token) => {
            println!("valid token: {}", token);
            Some(token)
        },
        database::MakeInitResult::New(key) => {
            println!("REGISTER ME key:{}", key);

            match database::activate_by_location(1, key.clone(), &connection ) {
                Ok(()) => {
                    println!("Activate the location! 1 for key {}", key);
                    None
                },
                Err(e) => {
                    println!("Got error on activate the location 1: {}", e);
                    None
                }
            }
        }
    };


    if t.is_some() {
        let token = t.unwrap();
        // now manual set time to now - 1 minute and try check_token
        let r = database::check_token(my_uuid.clone(), token.clone(),  &connection);

        assert_eq!(r, database::CheckToken::ValidToken);

        database::set_time_to_old(my_uuid.clone(), &connection);

        let r = database::check_token(my_uuid.clone(), token.clone(),  &connection);

        assert_eq!(r, database::CheckToken::ExpiredToken);

    }
    

}
