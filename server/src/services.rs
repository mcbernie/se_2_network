
#![deny(warnings, rust_2018_idioms)]

use std::sync::Arc;
use futures::{future};
use tower_grpc::{Request, Response};

use definitions::se2services::{
    server, 
    LoginRequest, LoginResponse, 
    PingRequest, PingResponse, 
    ConfigRequest, ConfigResponse,
    ListRequest, ListResponse,
};

use database::{DatabasePool, DatabaseResult, DatabaseErrors, MakeInitResult, UpdateCheckerResult};

use crate::clients::{Clients};

use tokio_trace::{field};

#[derive(Clone)]
pub struct Se2Service {
    db: Arc<DatabasePool>,
    clients: Arc<Clients>,
}

impl Se2Service {
    pub fn new() -> Self {
        Se2Service {
            db: Arc::new(DatabasePool::new()),
            clients: Arc::new(Clients::new()),
        }
    }

    pub fn clients(&self) -> Arc<Clients> {
        Arc::clone(&self.clients)
    }


    fn authenticate<T,C,R>(&mut self, request: Request<T>, f: C ) -> future::FutureResult<Response<R>, tower_grpc::Status>
        where C: FnOnce((i32, String, String)) -> future::FutureResult<Response<R>, tower_grpc::Status>
            
    {
        //let clients = self.clients.clone();
        let client_options = self.clients().check_if_client_authenticated(&request);
        match client_options {
            Some(client) => {

                match self.db.clone().check_if_token_is_valid(client.0, &client.2) {
                    Ok(_) => f(client),
                    Err(e) => {
                        error!("on check_if_token_is_valid, following error is raised: {:?}", e);
                        future::err(tower_grpc::Status::new(tower_grpc::Code::Unauthenticated, "Fehler"))
                    }
                }

            },
            None => {
                future::err(tower_grpc::Status::new(tower_grpc::Code::Unauthenticated, "Fehler"))
            }
        }
    }
}

impl server::Se2Services for Se2Service {
    type LoginFuture = future::FutureResult<Response<LoginResponse>, tower_grpc::Status>;

    fn login (&mut self, request: Request<LoginRequest>) -> Self::LoginFuture {
    
        let request = request.into_inner();

        debug!({ uuid = field::debug(request.uuid.clone()) }, "login request");

        let mut login_response  = LoginResponse::default();
        
        let clients = self.clients();
        match  self.db.clone().make_init(request.uuid.clone()) {
            Ok(DatabaseResult::MakeInitResult(init_result)) => {
                match init_result {
                    MakeInitResult::Valid(db_id, token) => {
                        if let Err(e) = clients.create(db_id, &request.uuid, &token) {
                            error!("could not create client {:?}", e);
                        } else {
                            info!("client successfull logged in");
                            login_response.set_type(definitions::se2services::login_response::LoginResponseType::Success);
                            login_response.token = token;
                        }
                    },
                    MakeInitResult::New(key) => {
                        info!( "client is new and got a key {:}", key);
                        login_response.set_type(definitions::se2services::login_response::LoginResponseType::New);
                        login_response.key = key;
                    }
                }
            },
            Ok(other) => {
                warn!("all other patterns should never been called! {:?}", other);
            }
            Err(e) => {
                match e {
                    DatabaseErrors::DeviceIsBlocked => {
                        info!("client is blocked");
                        login_response.set_type(definitions::se2services::login_response::LoginResponseType::Invalid);
                    },
                    DatabaseErrors::LocationNotFound => {
                        error!("Location for Device not found!");
                        login_response.set_type(definitions::se2services::login_response::LoginResponseType::Invalid);
                    },
                    _ => {
                        error!("error getting database data on login request: {:?} ", e);
                    }
                }
            }
        };

        //doe some magic login shit
        let response = Response::new(login_response);

        future::ok(response)
    }

    type PingFuture = future::FutureResult<Response<PingResponse>, tower_grpc::Status>;

    fn ping (&mut self, request: Request<PingRequest>) -> Self::PingFuture {
        debug!({ request = field::debug(&request) }, "ping request");

        let clients = self.clients();
        
        let db = self.db.clone();

        self.authenticate(request, |(db_id, uuid, token)| {
            info!("got ping from client {}, with token {}", uuid, token);
            // update client on database...
            db.update_device(db_id);
            
            // update client...
            clients.get(&token, |client| {
                match client.lock() {
                    Ok(mut client) => {
                        client.set_update_date();
                    }, 
                    Err(_) => {
                        warn!("could not write to client! {}", token);
                    }
                }
            });
            
            let mut ping_result = PingResponse::default();
           
            if let Ok(update) = db.check_for_updates(db_id) {
                match update {
                    UpdateCheckerResult::DataInDatabaseAreNewer => {
                        ping_result.set_update(definitions::se2services::ping_response::PingResponseType::Gotupdate);
                    },
                    _ => {}
                };
            }

            if let Ok(location) = db.get_location(db_id) {
                if let Ok(hash_value) = db.get_media_hash(location.tjms_hallenID.unwrap(), location.tjms_kundenID.unwrap()) {
                    clients.get(&token, |client| {
                        match client.lock() {
                            Ok(mut client) => {
                                if client.check_media_hash_updateable(hash_value) == true {
                                    info!("ping send, there is a update...");
                                    ping_result.set_update(definitions::se2services::ping_response::PingResponseType::Gotupdate);
                                }
                            }, 
                            Err(_) => {
                                warn!("could not write to client! {}", token);
                            }
                        }
                    });
                }
            }
            
            future::ok(Response::new(ping_result))
        })

    }

    type ConfigFuture = future::FutureResult<Response<ConfigResponse>, tower_grpc::Status>;

    fn config(&mut self, request: Request<ConfigRequest>) -> Self::ConfigFuture {
        debug!({ request = field::debug(&request) }, "config request");



        /*
        let style_testing = r#"{
            "scrolltext": {
                "background_start_color": [98, 98, 98],
                "background_end_color": [112, 112, 112],
                "background_alfa": 100,

                "text_color": [255,125,125],
                "text_alfa": 255,
                "text_border_color": [0,0,0],
                "text_border_alfa": 255,
                "text_border": true,
                "scrolltext_speed": 100.0,

                "padding_left": 120.0,
                "padding_right": 40.0,
                "padding_bottom": 40.0,

                "text_size": 46.0,
                "box_height": 200.0,

                "clock_background_color": [109, 72, 92],
                "clock_background_alfa": 255,
                "clock_color": [255,255,255],
                "clock_alfa": 255,
                "clock_width": 180.0,

                "stroke_on_top_color": [199, 199, 199],
                "stroke_on_top_alfa": 255,
                "stroke_on_top_width": 1.5
            }
        }"#;
        */


        let clients = self.clients();
        let db = self.db.clone();

        self.authenticate(request, |(db_id, uuid, token)| {
            info!("got config request from client {}, with token {}", uuid, token);
            
            db.update_device(db_id);

            clients.get(&token, |client| {
                match client.lock() {
                    Ok(mut client) => {
                        client.set_update_date();
                    }, 
                    Err(_) => {
                        warn!("could not write to client! {}", token);
                    }
                }
            });

            let mut config_response = ConfigResponse::default();

            match db.get_device(db_id) {
                Ok(device) => {
                    

                    match db.get_location(db_id) {
                        Ok(location) => {
                            info!("Set config for device.. response config");
                            config_response.statistic_pause = device.statistic_pause.unwrap_or(5);
                            config_response.slideshow_pause = device.slideshow_pause.unwrap_or(10);
                            config_response.name = location.name;
                            config_response.anschrift = location.anschrift;
                            config_response.show_clock = device.show_clock;
                            config_response.scrolltext = device.scrolltext.unwrap_or("".to_string());
                            config_response.raw_styles = base64::encode(&device.style.unwrap_or("".to_string()));
                        },
                        Err(e) => error!("=== error on get device: {:?}", e)
                    }
                },
                Err(e) => {
                    error!("=== error on get device: {:?}", e);
                }
            }


            future::ok(Response::new(config_response))
        })
    }


    type ListFuture = future::FutureResult<Response<ListResponse>, tower_grpc::Status>;

    fn list(&mut self, request: Request<ListRequest>) -> Self::ListFuture {
        debug!({ request = field::debug(&request) }, "list request");

        let clients = self.clients();
        let db = self.db.clone();

        self.authenticate(request, |(db_id, uuid, token)| {
            info!("got list request from client {}, with token {}", uuid, token);
            
            db.update_device(db_id);

            clients.get(&token, |client| {
                match client.lock() {
                    Ok(mut client) => {
                        client.set_update_date();
                    }, 
                    Err(_) => {
                        warn!("could not write to client! {}", token);
                    }
                }
            });

            let mut list_response = ListResponse::default();

            match db.get_device(db_id) {
                Ok(_device) => {
                    

                    match db.get_location(db_id) {
                        Ok(location) => {
                            info!("Set mediaList for device.. response mediaList");
                            match db.get_media(location.tjms_hallenID.unwrap_or(0), location.tjms_kundenID.unwrap_or(0)) {
                                Ok(medias) => {
                                    let mut counter = 0;
                                    
                                    list_response.items = medias.into_iter().map(|item| {
                                        counter += 1;
                                        definitions::se2services::Media { 
                                            name: item.Dateiname.clone(),
                                            uri: format!("http://wacogmbh.de:3999/index.php?m=fb&o=image&name={:}",item.Dateiname),
                                            position: counter,
                                            r#type: 0,
                                            duration: item.DefaultAnzeigedauerInSek,
                                            updated_on: 1,    
                                        }
                                    }).collect();

                                    counter += 1;
                                    list_response.items.push(definitions::se2services::Media { 
                                            name: "video_test_001".to_string(),
                                            uri: "http://itcoops.de/chuck_norris_1.mp4".to_string(),
                                            position: counter,
                                            r#type: 1,
                                            duration: 20,
                                            updated_on: 1,    
                                    })
                                },
                                Err(e) => {
                                    warn!("Could not receive medias! : {:?}", e)
                                }
                            }
                        },
                        Err(e) => error!("=== error on get device: {:?}", e)
                    }
                },
                Err(e) => {
                    error!("=== error on get device: {:?}", e);
                }
            }


            future::ok(Response::new(list_response))
        })
    }
    
}

