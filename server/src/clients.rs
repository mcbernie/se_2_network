#![allow(dead_code)]

use std::sync::{Arc, Mutex, RwLock};
use std::collections::HashMap;
use futures::{
    Future, 
    Stream
};

use failure::Fail;
use crate::helper;
use tower_grpc::{Request};

//use std::ops::Deref;

#[derive(Debug, Fail)]
pub enum ClientError {
    #[fail(display = "could not find client")]
    ClientNotFound,
    #[fail(display = "could not add client")]
    CouldNotAddClient,
    #[fail(display = "could not clean clients list")]
    CouldNotCleanClientsList,
    #[fail(display = "Error on Helper function: {:?}", _0)]
    HelperErrors(helper::HelperErrors),
}

impl From<helper::HelperErrors> for ClientError {
    fn from(err: helper::HelperErrors) -> Self {
        ClientError::HelperErrors(err)
    }
}


use chrono::{DateTime, Utc};
// Single Client Session
#[derive(Clone, Debug)]
pub struct Client {
    token: String,
    uuid: String,
    media_hash: u64,
    db_id: i32,
    created_on: DateTime<Utc>,
    updated_on: DateTime<Utc>,
}

impl Client {
    //get token
    pub fn token(&self) -> String {
        self.token.clone()
    }

    //get uuiod
    pub fn uuid(&self) -> String {
        self.uuid.clone()
    }

    pub fn set_update_date(&mut self) {
        self.updated_on = Utc::now();
    }

    pub fn check_media_hash_updateable(&mut self, media_hash: u64) -> bool {
        
        if self.media_hash == 0 {
            self.media_hash = media_hash;
            return false
        }

        if self.media_hash != media_hash {
            self.media_hash = media_hash;
            true
        } else {
            self.media_hash = media_hash;
            false
        }

    }
}


type ClientsList = Arc<RwLock<HashMap<String, Mutex<Client>>>>;

//#[derive(Clone)]
//Clients management and Client list
pub struct Clients(ClientsList);

impl Clients {
    pub fn new() -> Self {
        Clients(Arc::new(RwLock::new(HashMap::new())))
    }

    
    fn clients(&self) -> ClientsList {
        self.0.clone()
    }
    

    /// get a client by token
    pub fn get<F>(&self, token: &str, f: F )
    where F: FnOnce(&Mutex<Client>),
    {
        let clients = self.clients();
        let locked_clients = clients.read().expect("Should locked for reading");

        if let Some(element) = locked_clients.get(token) {
            f(element);
        }
    }

    pub fn check_if_client_authenticated<T>(&self, request: &Request<T> ) -> Option<(i32,String, String)> {
        match helper::get_token(request) {
            Ok(token) => {
                let clients = self.0.clone();
                let lock_clients = clients.read();
                match lock_clients {
                    Err(_) => None,
                    Ok(clients) => {
                        match clients.get(&token) {
                            Some(client) => {
                                match client.lock() {
                                    Ok(client) => Some((client.db_id, client.uuid.clone(), token)),
                                    Err(_) => None
                                }
                                
                            },
                            None => None
                        }
                    }
                }
            },
            Err(_e) => {
                None
            }
        }
        
    }

    /// add a new client to list
    pub fn create(&self, db_id: i32, uuid: &str, token: &str) -> Result<(),ClientError> {
        let c = Client{
            db_id, 
            token: token.to_string(), uuid: uuid.to_string(),
            created_on: Utc::now(),
            updated_on: Utc::now(), 
            media_hash: 0,
        };
        info!("add client to list");
        let clients = self.clients();
        let mut locked_clients = clients.write().expect("Should locked for reading");


        locked_clients.insert(token.to_string(), Mutex::new(c));

        Ok(())
    }


    /// timer / intervall for cleanup old clients
    pub fn clean_old_clients(&self) {
        use tokio::timer::Interval;
        use std::time::{Duration, Instant};
        
        let clients = self.0.clone();
        info!("clean_old_clients startup");
        let do_cleanup = Interval::new(Instant::now(), Duration::from_millis(24503))
            .for_each(move |_instant| {
                debug!("==> clean old clients...");
                if let Ok(mut clients) = clients.write() {
                    // remove old cleints and so on
                    //clients.clear();
                    //info!("Before: {:?}", clients);

                    let removing: Vec<_> = clients.iter().filter(|&(_key, client)| {
                        if let Ok(client) = client.lock() {
                            let duration = Utc::now().signed_duration_since(client.updated_on);
                            //info!("DURATION SINCE UPDATED_ON = : {:}", duration);
                            if duration.num_seconds() > 300 { // if client is older than 5 minutes
                                info!("cleanup client: {:?}", client);
                                return true
                            }
                            false
                        } else {
                            warn!("can check client, because its Mutex is Posioned");
                            false
                        }
                        
                    }).map(|(k, _)| k.clone())
                    .collect();


                    for remove in removing { clients.remove(&remove); }
                    //info!("Did we remove anything? {}", &removing.len());

                    //info!("After: {:?}", clients);

                }
                Ok(())
            }).map_err(|e| println!("error: {:?}", e));
        tokio::spawn(do_cleanup);
    }
}