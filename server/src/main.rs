#![deny(warnings, rust_2018_idioms)]

use definitions::se2services::{server};

use dotenv::dotenv;

mod helper;
mod clients;
mod services;
use services::Se2Service;




use futures::{
    Future, 
    Stream,
    lazy
};
//use log::error;
use tokio::net::TcpListener;
use tower_hyper::server::{Http, Server};

#[macro_use]
extern crate tokio_trace;




use tokio_trace::{field, Level};
use tokio_trace_futures::Instrument;

pub fn main() {
    dotenv().ok();
    let _ = ::env_logger::init();
    
    let subscriber = tokio_trace_fmt::FmtSubscriber::builder()
        // .with_filter(tokio_trace_fmt::filter::EnvFilter::from(
        //     "tower_h2_server=trace",
        // ))
        .full()
        .finish();

    tokio_trace::subscriber::with_default(subscriber, || {
        let service = Se2Service::new();
        let clients = service.clients();

        let new_service = server::Se2ServicesServer::new(service);

    
        let mut server = Server::new(new_service);
        let http = Http::new().http2_only(true).clone();
        
        let addr = "0.0.0.0:50051".parse().unwrap();
        let bind = TcpListener::bind(&addr).expect("bind"); 

        println!("micast HW Service is listening on {:?}", addr);


        let serve = bind
            .incoming()
            .for_each(move |sock| {
                if let Err(e) = sock.set_nodelay(true) {
                    return Err(e);
                }

                let addr = sock.peer_addr().expect("can't get addr");
                let conn_span = span!(
                    Level::INFO,
                    "conn",
                    remote_ip = field::debug(addr.ip()),
                    remote_port = addr.port() as u64,
                );
                let conn_span2 = conn_span.clone();
                conn_span.enter(|| {
                    let serve = server.serve_with(sock, http.clone())
                            .map_err(|e| println!("error {:?}", e))
                            .and_then(|_| {
                                debug!("response finished");
                                Ok(())
                            })
                        .instrument(conn_span2);
                    tokio::spawn(serve.map_err(|e| println!("h2 error: {:?}", e)));
                });

                Ok::<(), std::io::Error>(())
            })
            .map_err(|e| eprintln!("accept error: {}", e));

        tokio::run(lazy(move || {
            clients.clean_old_clients();
            serve
        }))

    });
}