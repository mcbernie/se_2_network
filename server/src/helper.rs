use tower_grpc::{Request};

const TOKEN_VAR:&'static str = "x-secure-token";

use failure::Fail;

#[derive(Debug, Fail)]
pub enum HelperErrors {
    #[fail(display = "could not find token in metadata")]
    TokenNotFound,
}


/// get a token from request metadata
pub fn get_token<T>(request: &Request<T>) -> Result<String, HelperErrors> {
    match request.metadata()
            .get(TOKEN_VAR)
            .and_then(|header| header.to_str().ok()) 
    {
        Some(token) => {
            Ok(token.to_string())
        },
        None => {
            Err(HelperErrors::TokenNotFound)
        }
    }
}